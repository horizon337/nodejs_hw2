const User = require('../models/user')
const { BadRequestError, NotFoundError } = require('../errors')
const getNoteById = require('../utils/getNoteById')

const getNotes = async (req, res) => {
  const { _id } = req.user


  const offset = Number(req.query.offset) || 0
  const limit = Number(req.query.limit) || 10

  const user = await User.findOne({ _id })
  const { notes } = user
  const limitedNotes = notes.slice(offset, limit)

  res.status(200).json({ offset, limit, count: limitedNotes.length, notes: limitedNotes })
}

const createNote = async (req, res) => {
  const { _id } = req.user

  if (Object.keys(req.body).length === 0) throw new BadRequestError('Please provide a valid note')
  const { text } = req.body

  if (text.trim().length === 0) throw new BadRequestError('Note should be at least one letter')

  const user = await User.findOne({ _id })
  const { notes } = user
  notes.push({ userId: _id, text })
  await User.findOneAndUpdate({ _id }, { notes })

  res.status(200).json({ message: 'Success! Note has been created.' })
}

const getNote = async (req, res) => {
  const { _id } = req.user
  const noteId = req.params.id

  let [notes, note, index] = await getNoteById(_id, noteId)

  if (!note) throw new NotFound('The note you are looking for does not exist')

  res.status(200).json({ note })
}

const editNote = async (req, res) => {
  const { _id } = req.user
  const noteId = req.params.id

  if (Object.keys(req.body).length === 0) throw new BadRequestError('Please provide a valid note')
  const { text } = req.body

  let [notes, note, index] = await getNoteById(_id, noteId)

  if (!note) throw new NotFound('The note you are looking for does not exist')

  note.text = text
  notes[index] = note
  await User.findOneAndUpdate({ _id }, { notes })

  res.status(200).json({ message: 'Success! Note has been edited.' })
}

const updateNote = async (req, res) => {
  const { _id } = req.user
  const noteId = req.params.id

  let [notes, note, index] = await getNoteById(_id, noteId)

  if (!note) throw new NotFound('The note you are looking for does not exist')

  note.completed = !note.completed
  notes[index] = note
  await User.findOneAndUpdate({ _id }, { notes })

  res.status(200).json({ message: 'Success! The note has been updated.' })
}

const deleteNote = async (req, res) => {
  const { _id } = req.user
  const noteId = req.params.id

  let [notes, note, index] = await getNoteById(_id, noteId)

  if (!note) throw new NotFound('The note you are looking for does not exist')

  notes.splice(index, 1)
  await User.findOneAndUpdate({ _id }, { notes })

  res.status(200).json({ message: 'Success! The note has been deleted.' })
}

module.exports = { getNotes, createNote, getNote, editNote, updateNote, deleteNote }
