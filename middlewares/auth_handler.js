const jwt = require('jsonwebtoken')
const { Unauthorized } = require('../errors')

const authMiddleware = async (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const decoded = jwt.verify(token, process.env.SECRET_KEY)

    const { _id, username, createdDate, password } = decoded
    req.user = { _id, username, createdDate, password }
    next()
  } catch (error) {
    throw new Unauthorized('Not authorized to access to this route')
  }
}

module.exports = authMiddleware
